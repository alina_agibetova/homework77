const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const db = require('../fileDb');

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },

  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const messages = [];
let messageId = 1;


router.get('/', (req, res) => {
  const messages = db.getItems();
  if (!messages){
    return res.status(404).send({message: 'Not found'});
  }
  return res.send(messages);
});

router.post('/', upload.single('image'), async (req,res,next) => {
  try {
    const message = {
      author: req.body.author,
      message: req.body.message,
    };

    if (req.file){
      message.image = req.file.filename;
    }

    await db.addItem(message);
    return res.send({message: 'Created new message with ID=' + message.id})
  } catch (e) {
    next(e);
  }

});

module.exports = router;

