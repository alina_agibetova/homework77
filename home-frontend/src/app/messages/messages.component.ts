import { Component, OnInit } from '@angular/core';
import { MessagesService } from '../services/messages.service';
import { Message } from '../models/message.model';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.sass']
})
export class MessagesComponent implements OnInit {
  messages: Message[] = [];
  constructor(private messageService: MessagesService) { }

  ngOnInit(): void {
    this.messageService.getMessages().subscribe( messages => {
      this.messages = messages;
    });
  }
}
