import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MessagesComponent } from './messages/messages.component';
import { ModalComponent } from './ui/modal/modal.component';

const routes: Routes = [
  {path: '', component: MessagesComponent},
  {path: 'messages/modal', component: ModalComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
