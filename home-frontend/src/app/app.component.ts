import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from './ui/modal/modal.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})

export class AppComponent {
  modalOpen = false;
  constructor(public dialog: MatDialog) {
  }


  openDialog() {
    this.dialog.open(ModalComponent);
  }
}



