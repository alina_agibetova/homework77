import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MessagesService } from '../../services/messages.service';
import { Router } from '@angular/router';
import { MessageData } from '../../models/message.model';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.sass']
})
export class ModalComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  constructor(private messageService: MessagesService, private router: Router, public dialog: MatDialog) { }

  onClose(){
    this.dialog.closeAll();
  }

  ngOnInit(): void {
  }

  onSubmit() {
    const value: MessageData = this.form.value;
      this.messageService.createMessage(value).subscribe(() => {
        void this.router.navigate(['/']);
        console.log(value);
      });
      this.messageService.getMessages();
    }
  }
